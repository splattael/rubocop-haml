# frozen_string_literal: true

require 'rubocop'

require_relative 'rubocop/haml'
require_relative 'rubocop/haml/version'
require_relative 'rubocop/haml/inject'

RuboCop::Haml::Inject.defaults!

require_relative 'rubocop/cop/haml_cops'
