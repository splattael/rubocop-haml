# frozen_string_literal: true

require 'haml_lint'

module RuboCop
  module Haml
    # Pre-processes HAML files and extracts Ruby code
    class Preprocess
      def initialize(path)
        @path = path
      end

      def call(source)
        config = {}
        document = HamlLint::Document.new(source, file: path, config: config)
        extracted = HamlLint::RubyExtractor.new.extract(document)

        puts extracted.source if ConfigLoader.debug?
        extracted.source
      end

      private

      attr_reader :path
    end
  end
end
