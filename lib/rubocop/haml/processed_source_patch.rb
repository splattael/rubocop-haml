# frozen_string_literal: true

require_relative 'preprocess'

module RuboCop
  module Haml
    # Extend ProcessedSource#parse with pre-processing
    module ProcessedSourcePatch
      # Overrdding instead of `initialize` to retain original HAML content in
      # `raw_source`.
      def parse(source, *args)
        # only process HAML files
        source = RuboCop::Haml::Preprocess.new(path).call(source) if path && RuboCop::Haml.haml_file?(path)

        super(source, *args)
      end
    end
  end
end
