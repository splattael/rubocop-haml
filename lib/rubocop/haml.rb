# frozen_string_literal: true

require_relative 'haml/version'
require_relative 'haml/processed_source_patch'

module RuboCop
  # RuboCop extension to check HAML.
  module Haml
    class Error < StandardError; end
    # Your code goes here...
    PROJECT_ROOT   = Pathname.new(__dir__).parent.parent.expand_path.freeze
    CONFIG_DEFAULT = PROJECT_ROOT.join('config', 'default.yml').freeze
    CONFIG         = YAML.safe_load(CONFIG_DEFAULT.read).freeze

    private_constant(:CONFIG_DEFAULT, :PROJECT_ROOT)

    def self.haml_file?(file)
      file.end_with?('.haml')
    end
  end
end

RuboCop::ProcessedSource.prepend RuboCop::Haml::ProcessedSourcePatch
