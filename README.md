# RuboCop::Haml

:warning: Proof of concepts. No tests yet. :warning:

TODO:
- Add tests
- Source code mapping?
- Add auto-correct ability
- Enable RuboCop caching

RuboCop extension to check HAML files.

Inspired by https://github.com/rubocop/rubocop-md. Uses https://github.com/sds/haml-lint.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rubocop-haml', require: false
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install rubocop-haml

## Usage

In your `.rubocop.yml`:

```yaml
require:
 - rubocop-haml

inherit_mode:
  merge:
    - Include
    - Exclude
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/splattael/rubocop-haml. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/splattael/rubocop-haml/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the RuboCop::Haml project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/splattael/rubocop-haml/blob/master/CODE_OF_CONDUCT.md).
